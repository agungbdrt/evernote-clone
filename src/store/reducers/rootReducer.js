import {combineReducers} from 'redux'
import {
    // ReactReduxFirebaseProvider,
    firebaseReducer
  } from 'react-redux-firebase'

import { firestoreReducer } from 'redux-firestore' // <- needed if using firestore
import noteReducer from './noteReducer'

const rootReducer = combineReducers({
    note: noteReducer,
    firebase: firebaseReducer,
    firestore: firestoreReducer // <- needed if using firestore
  })

  export default rootReducer