import firebase from 'firebase/app'
import 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyCo5MnmZ-XHpzxoAgYBt1P-30MvGl-rUCc",
    authDomain: "evernote-clone-4802d.firebaseapp.com",
    databaseURL: "https://evernote-clone-4802d.firebaseio.com",
    projectId: "evernote-clone-4802d",
    storageBucket: "evernote-clone-4802d.appspot.com",
    messagingSenderId: "701465743686",
    appId: "1:701465743686:web:a4d60c67252569f7751547"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  export default firebase
